import click
from loguru import logger

from my_project.my_module.utils import hello_world


@click.command()
def greet_me() -> None:
    logger.debug("I am Airflow Template!")
    hello_world()


if __name__ == "__main__":
    logger.info("Hello, World!")
