from loguru import logger


def hello_world() -> None:
    logger.info("Hello, World!")
