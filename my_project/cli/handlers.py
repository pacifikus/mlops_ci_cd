import typing as t
from typing_extensions import ParamSpec

import sys
from functools import wraps

import click

P = ParamSpec("P")
T = t.TypeVar("T")


def remove_from_argv(n_args: int) -> None:
    assert n_args >= 1
    assert n_args < len(sys.argv)
    sys.argv = [sys.argv[0]] + sys.argv[n_args + 1 :]


def cli_support(func: t.Callable[P, T]) -> t.Callable[P, None]:
    name = func.__name__

    @wraps(func)
    @click.command(name=name, context_settings={"ignore_unknown_options": True, "allow_extra_args": True})
    @click.pass_context
    def wrapper(ctx: click.Context, **options: t.Dict[str, t.Any]) -> None:
        remove_from_argv(1)
        func()

    return wrapper
