"""Command-line interface - root."""

from typing import Any, Dict

import sys

import click
from loguru import logger

from my_project.my_second_module.print_hello import greet_me

cmd_help = "CLI root."


@click.group(help=cmd_help)
@click.option(
    "-v",
    "--verbose",
    help="Enable verbose logging.",
    is_flag=True,
    default=False,
)
def cli_group(**options: Dict[str, Any]) -> None:
    """Define command-line interface root.

    Args:
        options (typing.Dict[str, typing.Any]): Map of command option names to
            their parsed values.

    """
    if options["verbose"]:
        logger.remove()
        logger.add(sys.stderr, level="DEBUG")
    else:
        logger.remove()
        logger.add(sys.stderr, level="INFO")


cli_group.add_command(greet_me, name="greet_me")
