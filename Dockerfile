ARG PYTHON_VERSION="3.11"

## Basic image with python and required ENVs
FROM python:$PYTHON_VERSION AS build-python-image

ARG SRC_PATH="/builds/"
COPY ./ $SRC_PATH/
WORKDIR $SRC_PATH

ENV POETRY_VIRTUALENVS_CREATE=true \
    POETRY_VIRTUALENVS_IN_PROJECT=true

RUN pip install --no-cache-dir poetry==1.8.1 \
    && poetry build \
    && . $SRC_PATH/.venv/bin/activate \
    && pip install --no-deps -v dist/*.whl

## Lightweighted final app image where venv directory with dependencies and app itself is copied from previous image
FROM python:$PYTHON_VERSION AS app-image

ARG SRC_PATH="/builds/"
ENV APP_VENV_PATH="$SRC_PATH/.venv" \
    PATH="$SRC_PATH/.venv/bin:$PATH"

COPY --from=build-python-image $APP_VENV_PATH $APP_VENV_PATH

WORKDIR $SRC_PATH

ENTRYPOINT [ "my_project" ]
CMD [ "--help" ]
