.EXPORT_ALL_VARIABLES:

POETRY_HOME ?= ${HOME}/.local/share/pypoetry
POETRY_BINARY ?= ${HOME}/.local/bin/poetry
POETRY_VERSION ?= 1.8.1

CODE = my_project tests

.PHONY: show-version
show-version:  ## Display version
	${POETRY_BINARY} version

.PHONY: build
build: ## Build project package
	echo "[build] Build project package."
	${POETRY_BINARY} build

.PHONY: build-image
build-image: ## Build docker image
	echo "[build-image] Build docker image."
	docker build -t project_image:local .

.PHONY: run-docker-image
run-docker-image: ## Run docker image
	echo "[run-docker-image] Build and run docker image."
	build-image
	docker run --name project --rm project_image:local

.PHONY: tests
tests: ## Run projects tests
	echo "[tests] Run projects tests."
	${POETRY_BINARY} run pytest tests

.PHONY: mypy
mypy:  ## Run projects mypy checks
	echo "[mypy] Run projects mypy checks."
	${POETRY_BINARY} run mypy --config-file lint.toml $(CODE)

.PHONY: lint
lint: mypy  ## Check linting code
